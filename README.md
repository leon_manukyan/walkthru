`walkthru` is a utility for executing (specially written) bash scripts in an
attended mode, where the user can see the commands to be executed before they
are executed. It is intended to facilitate the creation of demo/walkthrough
scripts for command line tools.

## Synopsis

    walkthru [--batch] <script> [<arg> ...]

## Examples

This repository contains example scripts in the `examples` subdirectory. They
can be executed via `walkthru` as follows:
 
    walkthru examples/printf-demo.sh

    walkthru examples/parameter-expansion-demo.sh qwertyuiop

## Execution

`walkthru` executes the input script block by block. Here *block* stands for a
contiguous range of non-empty lines. Each block is displayed as is (i.e. with
comments preserved and without any expansions performed) and the execution
pauses until the user hits the ENTER key (a corresponding hint is printed).
Then all commands in the displayed block are run.

## Limitations

- Each block must consist of a set of complete commands. For example, an `if`
  and its `else` cannot be separated into different blocks.
- The input script must not contain variables with names starting with
  `walkthru_script_` lest they clash with the internal variables of the
  `walkthru` script.
- If the input script enables bash's tracing mode (`set -x` or something similar)
  then the output will be clobbered with internals of the `walkthru` script.

## Similar projects

A similar tool is [demo-magic.sh](https://github.com/paxtonhare/demo-magic). The
difference of `walkthru` from `demo-magic.sh` is that the latter is more intrusive
and **requires** the demo scripts to be instrumented with special commands that it
defines. Unlike `demo-magic.sh`, demo scripts that are usable with `walkthru` can
also be executed directly and thus can serve other purposes too (for example, 
represent a test flow).