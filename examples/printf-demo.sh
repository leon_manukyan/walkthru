#!/usr/bin/env bash

# This demo contains some examples of the bash's printf builtin

# printf interprets escape sequences
printf 'ab\ncd\tef\n'

# printf can save the formatted output in a shell variable
# instead of sending it to standard output

# Let's make sure that the variable x is unset
unset x
echo $x

# Now, the usage of the -v option tells printf to assign
# the result to the variable x (without displaying anything)
printf -v x 'Hello, %s' world

# Let's check the contents of x
echo $x

# printf re-uses the format string as necessary to consume
# all of the arguments
printf "=== %s ===\n" abc xyz 123
