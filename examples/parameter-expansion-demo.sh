#!/usr/bin/env bash

# ${parameter:-word}
#           Use  Default  Values.   If  parameter is unset or null,
#           the expansion of word is substituted.  Otherwise, the value
#           of parameter is substituted.
x=${1:-walkthru}
echo "$x"

echo "length of '$x' is ${#x}"

echo "${x:1}"

echo "${x:1:5}"

echo "${x:1:-1}"
